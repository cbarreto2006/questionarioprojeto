/*
 * @author: Carlos A E Barreto
 * @version: 1.0
 * @creation 01/08/2017
 */
package com.teste.stream;

import java.util.Arrays;

import java.util.List;
import java.util.stream.Collectors;

public class TestStream {
	static ImplementaStream str = new ImplementaStream();
	public static void main(String[] args) {
		TestStream test = new TestStream();
        List<String> palavras = Arrays.asList(null // testa erros de convers�o
        		                              ,"" // testa erros vari�vel
        		                              , "tenis" //testa comportamento em minusculas 
        		                              , "NETSHOE" //palavras com grafia em ingles
        		                              ,"�" //testa acentua��o
        		                              ,"�" //testa acentua��o
        		                              ,"�" //testa acentua��o
        		                              ,"�" //testa acentua��o
        		                              ,"�" //testa acentua��o minusculas
        		                              ,"�" //testa acentua��o minusculas
        		                              ,"�" //testa acentua��o minusculas
        		                              ,"�" //testa acentua��o minusculas
        		                              ,"�"  //testa acentua��o
        		                              ,"�"  //testa acentua��o
        		                              ,"�"  //testa acentua��o
        		                              ,"�"  //testa acentua��o
        		                              ,"�"  //testa acentua��o
        		                              ,"�"  //testa acentua��o
        		                              ,"�"  //testa acentua��o
        		                              ,"�"  //testa acentua��o
        		                              ,"�"  //testa acentua��o
        		                              ,"�"  //testa acentua��o
        		                              ,"�"  //testa acentua��o
        		                              );

        List<String> result = palavras.stream()              // converte lista para stream
                .collect(Collectors.toList());              // coleta output e converte streams para a Lista de palavras digitadas
        
        for (int j = 0; j < result.size(); j++) {
        	test.validaVogal(result.get(j));
		}
        
                
    }


	private void validaVogal(Object objPalavra) {
	//le palavra e printa a primeira vogal se encontrada
		try {
			str.setWord(objPalavra.toString());
	        while(str.hasNext()) {
	        	if (str.isVogal()) {
	        		System.out.println("Palavra: "+str.getWord()+ "        vogal: " + str.getLetra());
	        		break; // encerra loop
	        	}
	        }
	        if (str.i==-1) {
	        	// se for nulo ou invalido o objeto
	        	System.out.println("Palavra � nula");
	        }else if (!str.isVogal()) {	
	        	// n�o possui vogal
	        	System.out.println("Palavra: "+str.getWord()+ "   n�o h� vogais");
	        }
	
		} catch (Exception e) {
			// se for nulo ou invalido o objeto
        	System.out.println("Palavra � nula");
		}
				
	}
}
