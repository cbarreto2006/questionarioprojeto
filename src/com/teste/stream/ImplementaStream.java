/*
 * @author: Carlos A E Barreto
 * @version: 1.0
 * @creation 01/08/2017
 */

package com.teste.stream;

public class ImplementaStream implements MyStream {

	String word;
	int i = 0;
	char letra;
	MyStream myStr=null;
	public char getLetra() {
		// retorna letra de leitura
		return letra;
	}

	public void setLetra(char letra) {
		// set letra em leitura
		this.letra = letra;
	}

	public boolean isVogal() {
		// set letra em leitura
		try {
			// se vogal confirma valida��o
			if ("A,E,I,O,U,�,�,�,�,�,�,�,�,�,�,�,�,�".indexOf(String.valueOf(letra).toUpperCase()) > -1) {
			    
				// seta retorno padr�o interface
				if ("�,�,�,�".indexOf(String.valueOf(letra).toUpperCase())>-1) {
					setLetra('A');
				}else if ("�,�".indexOf(String.valueOf(letra).toUpperCase())>-1) {
					setLetra('E');
				}else if ("�,�".indexOf(String.valueOf(letra).toUpperCase())>-1) {
					setLetra('I');
				}else if ("�,�,�".indexOf(String.valueOf(letra).toUpperCase())>-1) {
					setLetra('O');
				}else if ("�,�".indexOf(String.valueOf(letra).toUpperCase())>-1) {
					setLetra('U');
				}else {
					setLetra(String.valueOf(letra).toUpperCase().charAt(0));
				}
				
				MyStream.firstChar(myStr);
				return true;
			}

		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public String getWord() {
		// retorna palavra para processamento
		return word;
	}

	public void setWord(String word) {
		// recebe palavra para processamento
		this.word = word;
		try {
			// grava tamanho na primeira chamada
			i = word.length();
		} catch (Exception e) {
			i = -1;
		}

	}

	@Override
	public char getNext() {
		// Retona pr�ximo caracter lido da Stream 0-se fim e 1-se n�o
		try {
			// get susbstring ultimo para o primeiro
			letra = getWord().charAt(--i);
			setLetra(letra);
			return 1;
		} catch (Exception e) {
			return 0;
		}

	}

	@Override
	public boolean hasNext() {
		// Valida se pr�ximo caracter pode ser lido
		if (getNext() == 1) {
			return true;
		}
		return false;
	}

}
