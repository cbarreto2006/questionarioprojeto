/*
 * @author: Carlos A E Barreto
 * @version: 1.0
 * @creation 01/08/2017
 */
package com.teste.stream;

public interface MyStream {
	
		public char getNext();
		public boolean hasNext();
		public static char firstChar(MyStream input) {
			return 0;
		}
}
